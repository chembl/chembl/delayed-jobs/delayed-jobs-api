#!/usr/bin/env python3
"""
Script that runs the daemon that checks for the job status
"""
import time
import argparse

from app.job_status_daemon import daemon
from app import create_app
from app.models import delayed_job_models

PARSER = argparse.ArgumentParser()
PARSER.add_argument('job_id', help='id of the job to check')
ARGS = PARSER.parse_args()


def run():
    flask_app = create_app()
    job_id = ARGS.job_id
    with flask_app.app_context():
        job = delayed_job_models.get_job_by_id(job_id, force_refresh=True)
        while job.needs_to_be_checked_in_lsf():
            sleep_time, jobs_were_checked = daemon.check_job_status(job_id)
            time.sleep(sleep_time)
            job = delayed_job_models.get_job_by_id(job_id, force_refresh=True)


if __name__ == "__main__":
    run()
