"""
This Module tests the basic functions of the status daemon
"""
import unittest
import socket
from pathlib import Path
from datetime import datetime, timedelta
from os import path
import shutil
import os

from app import create_app
from app.models import delayed_job_models
from app.config import RUN_CONFIG
from app.job_status_daemon import daemon
from app.blueprints.job_submission.services import job_submission_service
from app.job_status_daemon import locks
from app import utils


class TestJobStatusDaemon(unittest.TestCase):
    """
    Class to test Job Status Daemon
    """

    def setUp(self):
        self.flask_app = create_app()
        self.client = self.flask_app.test_client()

        with self.flask_app.app_context():
            delayed_job_models.delete_all_jobs()
            shutil.rmtree(daemon.AGENT_RUN_DIR, ignore_errors=True)

    def tearDown(self):
        with self.flask_app.app_context():
            delayed_job_models.delete_all_jobs()
            shutil.rmtree(daemon.AGENT_RUN_DIR, ignore_errors=True)

    def create_test_jobs_0(self):
        """
        This will create:
        - 2 Jobs in created state, each running in a different lsf cluster
        - 2 Jobs in queued state, each running in a different lsf cluster
        - 2 Jobs in running state, each running in a different lsf cluster
        - 2 Jobs in error state, each running in a different lsf cluster
        - 2 Jobs in finished state, each running in a different lsf cluster
        """
        lsf_config = RUN_CONFIG.get('lsf_submission')
        lsf_host = lsf_config['lsf_host']

        run_environment = RUN_CONFIG.get('run_env')

        with self.flask_app.app_context():

            i = 0
            for status in [delayed_job_models.JobStatuses.CREATED, delayed_job_models.JobStatuses.QUEUED,
                           delayed_job_models.JobStatuses.RUNNING, delayed_job_models.JobStatuses.FINISHED,
                           delayed_job_models.JobStatuses.ERROR]:

                for assigned_host in [lsf_host, 'another_host']:
                    job = delayed_job_models.DelayedJob(
                        id=f'Job-{assigned_host}-{status}',
                        type='TEST',
                        lsf_job_id=i,
                        status=status,
                        lsf_host=assigned_host,
                        run_environment=run_environment,
                        created_at=datetime.utcnow(),
                        started_at=datetime.utcnow() + timedelta(seconds=1),
                        finished_at=datetime.utcnow() + timedelta(seconds=2),
                    )
                    job.output_dir_path = job_submission_service.get_job_output_dir_path(job)
                    os.makedirs(job.output_dir_path, exist_ok=True)
                    delayed_job_models.save_job(job)
                    i += 1

    def create_test_jobs_1(self):
        """
        This will create:
        - 2 Jobs in error state, each running in a different lsf cluster
        - 2 Jobs in finished state, each running in a different lsf cluster
        """
        run_environment = RUN_CONFIG.get('run_env')
        lsf_config = RUN_CONFIG.get('lsf_submission')
        lsf_host = lsf_config['lsf_host']

        with self.flask_app.app_context():

            i = 0
            for status in [delayed_job_models.JobStatuses.FINISHED,
                           delayed_job_models.JobStatuses.ERROR]:

                for assigned_host in [lsf_host, 'another_host']:
                    job = delayed_job_models.DelayedJob(
                        id=f'Job-{assigned_host}-{status}',
                        type='TEST',
                        lsf_job_id=i,
                        status=status,
                        lsf_host=assigned_host,
                        run_environment=run_environment,
                        created_at=datetime.utcnow(),
                        started_at=datetime.utcnow() + timedelta(seconds=1),
                        finished_at=datetime.utcnow() + timedelta(seconds=2)
                    )
                    job.output_dir_path = job_submission_service.get_job_output_dir_path(job)
                    os.makedirs(job.output_dir_path, exist_ok=True)
                    delayed_job_models.save_job(job)
                    i += 1

    def create_test_jobs_2(self):
        """
        This will create:
        - 2 Jobs in created state, each running in a different run environment
        - 2 Jobs in queued state, each running in a different run environment
        - 2 Jobs in running state, each running in a different run environment
        - 2 Jobs in error state, each running in a different run environment
        - 2 Jobs in finished state, each running in a different run environment
        """
        lsf_config = RUN_CONFIG.get('lsf_submission')
        lsf_host = lsf_config['lsf_host']

        run_environment = RUN_CONFIG.get('run_env')

        with self.flask_app.app_context():

            i = 0
            for status in [delayed_job_models.JobStatuses.CREATED, delayed_job_models.JobStatuses.QUEUED,
                           delayed_job_models.JobStatuses.RUNNING, delayed_job_models.JobStatuses.FINISHED,
                           delayed_job_models.JobStatuses.ERROR]:

                for run_env in [run_environment, 'another_environment']:
                    job = delayed_job_models.DelayedJob(
                        id=f'Job-{status}-{run_env}',
                        type='TEST',
                        lsf_job_id=i,
                        status=status,
                        lsf_host=lsf_host,
                        run_environment=run_env,
                        created_at=datetime.utcnow(),
                        started_at=datetime.utcnow() + timedelta(seconds=1),
                        finished_at=datetime.utcnow() + timedelta(seconds=2)
                    )
                    job.output_dir_path = job_submission_service.get_job_output_dir_path(job)
                    os.makedirs(job.output_dir_path, exist_ok=True)
                    delayed_job_models.save_job(job)
                    i += 1

    def test_produces_a_correct_job_status_check_script_path(self):
        """
        Test that produces a correct path for the job status script
        """

        job_id = 'job_1'
        lsf_job_id = 1
        filename = f'{utils.get_utc_now().strftime("%Y-%m-%d-%H-%M-%S")}_job_id-{job_id}_lsf_job_id-{lsf_job_id}' \
                   f'_check_lsf_job_status.sh'
        job_status_check_script_path_must_be = Path(daemon.AGENT_RUN_DIR).joinpath(socket.gethostname(), filename)
        print('job_status_check_script_path_must_be: ', job_status_check_script_path_must_be)

        job_status_check_script_path_got = daemon.get_check_job_status_script_path(job_id=job_id, lsf_job_id=lsf_job_id)

        # remove the last character (the second) to avoid annoying false negatives
        self.assertEqual(str(job_status_check_script_path_must_be)[:-1], str(job_status_check_script_path_got)[:-1],
                         msg='The path for the job status checking job was not produced correctly!')

    def test_prepares_the_job_status_script(self):
        """
        Test that the job status script is created and can be executed.
        """
        job_id = 'job_1'
        lsf_job_id = 1

        with self.flask_app.app_context():
            script_path_got = daemon.prepare_job_status_check_script(job_id=job_id, lsf_job_id=lsf_job_id)
            self.assertTrue(path.isfile(script_path_got), msg='The job status check script has not been created!')

            self.assertTrue(os.access(script_path_got, os.X_OK),
                            msg=f'The script file for the job ({script_path_got}) is not executable!')

    def load_sample_file(self, file_path):
        """
        Loads a file with a sample read from the path specified as a parameter
        :param file_path: path of the sample
        :return: a string with the contents of the file
        """
        sample_output_file_path = Path(file_path).resolve()
        with open(sample_output_file_path, 'rt') as sample_output_file:
            sample_output = sample_output_file.read()
            return sample_output

    def test_parses_the_output_of_bjobs_when_no_jobs_were_found(self):
        """
        Generates mock jobs, then sends a mock output to the the function to test that it interpreted the output
        accordingly
        """
        self.create_test_jobs_0()
        sample_output = self.load_sample_file('app/job_status_daemon/test/data/sample_lsf_output_0.txt')

        with self.flask_app.app_context():
            daemon.parse_bjobs_output(sample_output)
            # No status should have changed

            for status_must_be in [delayed_job_models.JobStatuses.CREATED, delayed_job_models.JobStatuses.QUEUED,
                                   delayed_job_models.JobStatuses.RUNNING, delayed_job_models.JobStatuses.FINISHED,
                                   delayed_job_models.JobStatuses.ERROR]:

                lsf_config = RUN_CONFIG.get('lsf_submission')
                lsf_host = lsf_config['lsf_host']

                for assigned_host in [lsf_host, 'another_host']:
                    id_to_check = f'Job-{assigned_host}-{status_must_be}'
                    job = delayed_job_models.get_job_by_id(id_to_check)
                    status_got = job.status
                    self.assertEqual(status_got, status_must_be,
                                     msg='The status was modified! This should have not modified the status')

    def test_parses_the_output_of_bjobs_running_job(self):
        """
        Generates mock jobs, then sends a mock output to the the function to test that it interpreted the output
        accordingly. This test focuses on a job that switched to running state.
        """
        self.create_test_jobs_0()

        sample_output = self.load_sample_file('app/job_status_daemon/test/data/sample_lsf_output_1.txt')

        with self.flask_app.app_context():
            daemon.parse_bjobs_output(sample_output)
            # job with lsf id 0 should be in running state now
            lsf_job_id = 0
            job = delayed_job_models.get_job_by_lsf_id(lsf_job_id)
            status_got = job.status
            status_must_be = delayed_job_models.JobStatuses.RUNNING
            self.assertEqual(status_got, status_must_be, msg='The status of the job was not changed accordingly!')

    def test_parses_the_output_of_bjobs_pending_job(self):
        """
        Generates mock jobs, then sends a mock output to the the function to test that it interpreted the output
        accordingly. This test focuses on a job that switched to pending state.
        """
        self.create_test_jobs_0()

        sample_output = self.load_sample_file('app/job_status_daemon/test/data/sample_lsf_output_2.txt')

        with self.flask_app.app_context():
            daemon.parse_bjobs_output(sample_output)
            # job with lsf id 0 should be in running state now
            lsf_job_id = 0
            job = delayed_job_models.get_job_by_lsf_id(lsf_job_id)
            status_got = job.status
            status_must_be = delayed_job_models.JobStatuses.QUEUED
            self.assertEqual(status_got, status_must_be, msg='The status of the job was not changed accordingly!')

    def test_parses_the_output_of_bjobs_error_job(self):
        """
        Generates mock jobs, then sends a mock output to the the function to test that it interpreted the output
        accordingly. This test focuses on a job that switched to error state.
        """
        self.create_test_jobs_0()

        sample_output = self.load_sample_file('app/job_status_daemon/test/data/sample_lsf_output_1.txt')

        with self.flask_app.app_context():
            daemon.parse_bjobs_output(sample_output)
            # job with lsf id 0 should be in running state now
            lsf_job_id = 2
            job = delayed_job_models.get_job_by_lsf_id(lsf_job_id)
            status_got = job.status
            status_must_be = delayed_job_models.JobStatuses.ERROR
            self.assertEqual(status_got, status_must_be, msg='The status of the job was not changed accordingly!')

    def test_parses_the_output_of_bjobs_job_not_found(self):
        """
        Generates mock jobs, then sends a mock output to the the function to test that it interpreted the output
        accordingly. This test focuses on an error on the system but still gives output
        """
        self.create_test_jobs_0()

        sample_output = self.load_sample_file('app/job_status_daemon/test/data/sample_lsf_output_3.txt')
        with self.flask_app.app_context():
            daemon.parse_bjobs_output(sample_output)

            lsf_job_id = 2
            job = delayed_job_models.get_job_by_lsf_id(lsf_job_id)
            status_got = job.status
            status_must_be = delayed_job_models.JobStatuses.UNKNOWN
            self.assertEqual(status_got, status_must_be, msg='The status of the job was not changed accordingly!')

    def test_parses_the_output_of_bjobs_finished_job(self):
        """
        Generates mock jobs, then sends a mock output to the the function to test that it interpreted the output
        accordingly. This test focuses on a job that switched to finished state.
        """
        self.create_test_jobs_0()

        sample_output = self.load_sample_file('app/job_status_daemon/test/data/sample_lsf_output_1.txt')

        with self.flask_app.app_context():
            daemon.parse_bjobs_output(sample_output)
            # job with lsf id 0 should be in running state now
            lsf_job_id = 4
            job = delayed_job_models.get_job_by_lsf_id(lsf_job_id)
            status_got = job.status
            status_must_be = delayed_job_models.JobStatuses.FINISHED
            self.assertEqual(status_got, status_must_be, msg='The status of the job was not changed accordingly!')

            finished_time = job.finished_at

            delta = timedelta(days=RUN_CONFIG.get('job_expiration_days'))
            expiration_date_must_be = finished_time + delta
            expiration_date_must_be_timestamp = expiration_date_must_be.timestamp()

            expiration_date_got = job.expires_at
            expiration_date_got_timestamp = expiration_date_got.timestamp()

            self.assertAlmostEqual(expiration_date_got_timestamp, expiration_date_must_be_timestamp,
                                   msg='The expiration date was not calculated correctly', delta=1)

    def test_collects_the_urls_for_the_outputs_of_a_finished_job(self):
        """
        Generates some mock jobs, then sends a mock output to the function to test that it interprets that it finished.
        The finished job should have now the output files set
        """
        self.create_test_jobs_0()

        sample_output = self.load_sample_file('app/job_status_daemon/test/data/sample_lsf_output_1.txt')

        with self.flask_app.app_context():
            # Prepare the test scenario
            lsf_job_id = 4
            job = delayed_job_models.get_job_by_lsf_id(lsf_job_id)

            output_urls_must_be = []

            for i in range(0, 2):

                for subdir in ['', 'subdir/']:

                    out_file_name = f'output_{i}.txt'
                    out_file_path = f'{job.output_dir_path}/{subdir}{out_file_name}'
                    os.makedirs(Path(out_file_path).parent, exist_ok=True)
                    with open(out_file_path, 'wt') as out_file:
                        out_file.write(f'This is output file {i}')

                    server_base_path = RUN_CONFIG.get('base_path', '')
                    if server_base_path == '':
                        server_base_path_with_slash = ''
                    else:
                        server_base_path_with_slash = f'{server_base_path}/'

                    outputs_base_path = RUN_CONFIG.get('outputs_base_path')
                    output_url_must_be = f'/{server_base_path_with_slash}{outputs_base_path}/' \
                                         f'{job.id}/{subdir}{out_file_name}'

                    output_urls_must_be.append(output_url_must_be)

            # END to prepare the test scenario

            daemon.parse_bjobs_output(sample_output)
            job_outputs_got = job.output_files
            self.assertEqual(len(job_outputs_got), 4, msg='There must be 4 outputs for this job!')

            for output_file in job.output_files:
                output_url_got = output_file.public_url
                self.assertIn(output_url_got, output_urls_must_be, msg='The output url was not set correctly')

    def test_daemon_creates_lock_when_checking_lsf(self):
        """
        Tests that the daemon creates a lock while checking LSF
        """
        self.create_test_jobs_0()
        lsf_config = RUN_CONFIG.get('lsf_submission')
        lsf_host = lsf_config['lsf_host']
        my_hostname = socket.gethostname()
        job_id_to_check = f'Job-{lsf_host}-{delayed_job_models.JobStatuses.QUEUED}'

        with self.flask_app.app_context():
            daemon.check_job_status(job_id=job_id_to_check, delete_lock_after_finishing=False)
            current_lsf_host = RUN_CONFIG.get('lsf_submission').get('lsf_host')

            lock_hostname_got = locks.get_lock_for_job(job_id_to_check)
            self.assertIsNotNone(lock_hostname_got, msg='The lock was not created!')
            lock_hostname_must_be = socket.gethostname()
            self.assertEqual(lock_hostname_got, lock_hostname_must_be, msg='The lock was not saved correctly!')
            locks.delete_lock_for_job(job_id_to_check)

    def test_agent_respects_a_lock(self):
        """
        Tests that when a lock has been created for another host, the agent respects it. This means that
        the agent does not check anything in lsf
        """
        self.create_test_jobs_0()
        lsf_config = RUN_CONFIG.get('lsf_submission')
        lsf_host = lsf_config['lsf_host']
        job_id_to_check = f'Job-{lsf_host}-{delayed_job_models.JobStatuses.QUEUED}'

        with self.flask_app.app_context():
            locks.set_lock_for_job(job_id_to_check, 'another_owner')

            sleep_time_got, jobs_were_checked = daemon.check_job_status(job_id_to_check)
            self.assertFalse(jobs_were_checked, msg='The jobs should have not been checked')

            sleep_time = RUN_CONFIG.get('status_agent').get('sleep_time')
            self.assertTrue(sleep_time_got == sleep_time,
                            msg='The sleep time was not calculated correctly!')

            locks.delete_lock_for_job(job_id_to_check)

    def test_deletes_lock_after_finishing(self):
        """
        Tests that it requests the deletion of the lock after checking the jobs
        """
        self.create_test_jobs_0()
        lsf_config = RUN_CONFIG.get('lsf_submission')
        lsf_host = lsf_config['lsf_host']
        job_id_to_check = f'Job-{lsf_host}-{delayed_job_models.JobStatuses.QUEUED}'

        with self.flask_app.app_context():
            daemon.check_job_status(job_id_to_check)

            lock_got = locks.get_lock_for_job(job_id_to_check)
            self.assertIsNone(lock_got, msg='The LSF lock was not deleted!')
