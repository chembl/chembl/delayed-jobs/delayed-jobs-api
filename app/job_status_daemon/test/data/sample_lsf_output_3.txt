I am going to check the status of the LSF jobs 0 2 4
START_REMOTE_SSH
------------------------------------------------------------------------------------
--                                                                                --
--                        Welcome to the EBI                                      --
--                                                                                --
--                _       _                                                       --
--               | |__   | |__           __      ___ __                           --
--               | '_ \  | '_ \   _____  \ \ /\ / / '_ \                          --
--               | | | | | | | | |_____|  \ V  V /| |_) |                         --
--               |_| |_| |_| |_|           \_/\_/ | .__/                          --
--                                                |_|                             --
--                                                                                --
--                      High Performance Cluster                                  --
--                                                                                --
--                                                                                --
--   - /nfs/public/rw     Read-Write Wep-Prudction storage                        --
--   - /nfs/public/ro     Read-Only Wep-Prudction storage                         --
--                                                                                --
--                                                                                --
--   More info:                                                                   --
--  https://embl.service-now.com/sp?id=kb_article_view&sysparm_article=KB0010969  --
--                                                                                --
--                                                                                --
--                                                                                --
--   For any issues or questions:                                                 --
--       - ticketing system:                                                      --
--  https://embl.service-now.com/sp?id=kb_article_view&sysparm_article=KB0010612  --
--       https://embl.service-now.com/                                            --
--                                                                                --
--                                                                                --
--                                                                                --
------------------------------------------------------------------------------------
Activate the web console with: systemctl enable --now cockpit.socket

{
  "COMMAND":"bjobs",
  "JOBS":20,
  "RECORDS":[
    {
      "JOBID":"0",
      "ERROR":"Job <304093> is not found"
    },
    {
      "JOBID":"2",
      "ERROR":"Job <305851> is not found"
    },
    {
      "JOBID":"4",
      "ERROR":"Job <307565> is not found"
    }
  ]
}