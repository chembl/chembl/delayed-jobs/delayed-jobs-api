"""
Module that handles the locking system for the status daemons
"""
from app.models import delayed_job_models


def get_lock_for_job(job_id):
    """
    Returns a lock for the job if it exists
    :param job_id: id of the job for which to get the lock
    """
    job = delayed_job_models.get_job_by_id(job_id)
    return job.lsf_check_lock_owner


def set_lock_for_job(job_id, lock_owner):
    """
    Creates a lock on the job with job_id given as parameter in the name of the owner given as parameter, it will expire
    in the time set up in the configuration, set by the value status_agent.lock_validity_seconds
    :param job_id: id of the job
    :param lock_owner: owner of the lock, typically the hostname of the pod
    """
    job = delayed_job_models.get_job_by_id(job_id)
    job.lsf_check_lock_owner = lock_owner
    delayed_job_models.save_job(job)


def delete_lock_for_job(job_id):
    """
    Deletes the lock for the job with id passed as parameter
    :param job_id: id of the job
    """
    job = delayed_job_models.get_job_by_id(job_id)
    job.lsf_check_lock_owner = None
    delayed_job_models.save_job(job)
