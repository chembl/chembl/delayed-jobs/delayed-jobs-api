"""
Module that provide services for administrative tasks of the system
"""
import os
from pathlib import Path
import shutil

from flask import abort

from app.models import delayed_job_models
from app.job_status_daemon import daemon
from app.config import RUN_CONFIG


class JobNotFoundError(Exception):
    """Base class for exceptions."""


def delete_all_jobs_by_type(job_type):
    """
    Triggers the deletion of the jobs of the given type
    :param job_type: type of the job to delete
    :return: a message (string) with the result of the operation
    """
    num_deleted = delayed_job_models.delete_all_jobs_by_type(job_type)
    return f'All {num_deleted} jobs of type {job_type} were deleted!'


def delete_all_outputs_of_job(job_id):
    """
    Deletes all the outputs of the job of the job given as parameter. The job must be in FINISHED state
    :param job_id: id of the job for which the outputs will be deleted.
    :return:
    """
    try:
        job = delayed_job_models.get_job_by_id(job_id)
        output_dir_path = job.output_dir_path

        for item in os.listdir(output_dir_path):
            item_path = str(Path(output_dir_path).joinpath(item))
            try:
                shutil.rmtree(item_path)
            except NotADirectoryError:
                os.remove(item_path)

        return f'All outputs of the job {job_id} were deleted!'
    except delayed_job_models.JobNotFoundError:
        raise abort(404)


def delete_job_by_id(job_id):
    """
    Deletes the job by the given ID
    :param job_id: id of the job to delete
    :return:
    """
    try:
        job = delayed_job_models.get_job_by_id(job_id)
        delayed_job_models.delete_job(job)

        return f'The job with id {job_id} was deleted!'
    except delayed_job_models.JobNotFoundError:
        raise abort(404)


def delete_job_by_lsf_job_id(lsf_job_id):
    """
    Deletes the job by the given ID
    :param lsf_job_id: LSF id of the job to delete
    :return:
    """
    try:
        job = delayed_job_models.get_job_by_lsf_job_id(lsf_job_id)
        delayed_job_models.delete_job(job)

        return f'The job with LSF id {job_id} was deleted!'
    except delayed_job_models.JobNotFoundError:
        raise abort(404)


def delete_expired_jobs():
    """
    Deletes all the jobs that have expired
    :return: a message (string) with the result of the operation
    """
    num_deleted = delayed_job_models.delete_all_expired_jobs()
    # now that all the jobs have a default expiration date, jobs with no expiration date are not allowed
    num_deleted_with_no_expiration_date = delayed_job_models.delete_jobs_with_no_expiration_date()
    num_daemon_scripts_deleted = daemon.delete_old_daemon_files()
    # also delete jobs that are stuck
    num_stuck_jobs_deleted = delayed_job_models.delete_all_stuck_jobs()

    daemon_scripts_expiration_minutes = RUN_CONFIG.get('daemon_scripts_expiration_minutes')
    return f'Deleted:\n' \
           f'- {num_deleted} expired jobs.\n' \
           f'- {num_daemon_scripts_deleted} daemon scripts which expired ' \
           f'{daemon_scripts_expiration_minutes} minutes ago or earlier.\n' \
           f'- {num_deleted_with_no_expiration_date} jobs with no expiration date' \
           f'- {num_stuck_jobs_deleted} that were stuck'
