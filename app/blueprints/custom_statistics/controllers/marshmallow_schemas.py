"""
Schemas to validate the input of custom statistics endpoint
"""
from marshmallow import Schema, fields, validate


class JobID(Schema):
    """
    Class that the schema for identifying the job
    """
    job_id = fields.String(required=True)


class TestJobStatistics(Schema):
    """
    Class that the schema for saving statistics for the test jobs
    """
    duration = fields.Number(required=True, validate=validate.Range(min=0))


class StructureSearchJobStatistics(Schema):
    """
    Class that the schema for saving statistics for the structure search jobs
    """
    search_type = fields.String(required=True, validate=validate.OneOf(['SIMILARITY', 'SUBSTRUCTURE', 'CONNECTIVITY']))
    time_taken = fields.Number(required=True, validate=validate.Range(min=0))


class BiologicalSequenceSearchJobStatistics(Schema):
    """
    Class that the schema for saving statistics for the biological sequence search jobs
    """
    time_taken = fields.Number(required=True, validate=validate.Range(min=0))


class MMVSearchJobStatistics(Schema):
    """
    Class that the schema for saving statistics for the structure search jobs
    """
    num_sequences = fields.Number(required=True, validate=validate.Range(min=0))


class DownloadJobStatistics(Schema):
    """
    Class that the schema for saving statistics for the structure search jobs
    """
    time_taken = fields.Number(required=True, validate=validate.Range(min=0))
    desired_format = fields.String(required=True)
    file_size = fields.Number(required=True, validate=validate.Range(min=0))
    es_index = fields.String(required=True)
    es_query = fields.String(required=True)
    total_items = fields.Number(required=True, validate=validate.Range(min=0))


class SearchByIDsJobStatistics(Schema):
    """
    Class that the schema for saving statistics for the search by ids jobs
    """
    time_taken = fields.Number(required=True, validate=validate.Range(min=0))
    entity_from = fields.String(required=True)
    entity_to = fields.String(required=True)
    separator = fields.String(required=True)
    total_ids_entered = fields.Number(required=True, validate=validate.Range(min=0))
    total_unique_ids = fields.Number(required=True, validate=validate.Range(min=0))
    duplicate_ids_found = fields.Number(required=True, validate=validate.Range(min=0))
    num_items_with_match = fields.Number(required=True, validate=validate.Range(min=0))
    num_items_with_no_match = fields.Number(required=True, validate=validate.Range(min=0))
    origin_index = fields.String(required=True)
    subset_index_name = fields.String(required=True)
    num_uncompressed_bytes = fields.Number(required=True, validate=validate.Range(min=0))
    num_compressed_bytes = fields.Number(required=True, validate=validate.Range(min=0))
    compression_ratio = fields.Number(required=True, validate=validate.Range(min=0))
    step_1_time_taken = fields.Number(required=True, validate=validate.Range(min=0))
    step_2_time_taken = fields.Number(required=True, validate=validate.Range(min=0))
    step_3_time_taken = fields.Number(required=True, validate=validate.Range(min=0))
    step_4_time_taken = fields.Number(required=True, validate=validate.Range(min=0))
    step_5_time_taken = fields.Number(required=True, validate=validate.Range(min=0))
    num_obsolete_items = fields.Number(required=True, validate=validate.Range(min=0))
    num_other_excluded_items = fields.Number(required=True, validate=validate.Range(min=0))
    num_ids_included_in_results = fields.Number(required=True, validate=validate.Range(min=0))
    num_inactive_items = fields.Number(required=True, validate=validate.Range(min=0))
