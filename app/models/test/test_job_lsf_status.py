"""
Tests related to the status of the job in LSF
"""
import unittest
import datetime

from app import create_app
from app.models import delayed_job_models
from app.config import RUN_CONFIG
from app import utils


class TestJobLSFStatus(unittest.TestCase):
    """
    Class to test the columns related to the job lsf status checking
    """

    def setUp(self):
        self.flask_app = create_app()
        self.client = self.flask_app.test_client()

    def tearDown(self):
        with self.flask_app.app_context():
            delayed_job_models.delete_all_jobs()

    def test_determines_when_a_job_needs_to_be_checked(self):
        """
        Test that determines when a job needs to be checked in LSF because of its status
        and when it does noe need to be checked
        """

        with self.flask_app.app_context():
            job_type = 'SIMILARITY'
            params = {
                'search_type': 'SIMILARITY',
                'structure': '[H]C1(CCCN1C(=N)N)CC1=NC(=NO1)C1C=CC(=CC=1)NC1=NC(=CS1)C1C=CC(Br)=CC=1',
                'threshold': '70'
            }
            docker_image_url_must_be = 'some_url'
            job = delayed_job_models.get_or_create(job_type, params, docker_image_url_must_be)
            for status in [delayed_job_models.JobStatuses.QUEUED, delayed_job_models.JobStatuses.RUNNING,
                           delayed_job_models.JobStatuses.UNKNOWN]:
                job.status = status
                needs_to_be_checked_got = job.needs_to_be_checked_in_lsf()
                self.assertTrue(needs_to_be_checked_got, msg=f'A job with status {status} need to be checked in LSF!')

            for status in [delayed_job_models.JobStatuses.CREATED, delayed_job_models.JobStatuses.ERROR,
                           delayed_job_models.JobStatuses.FINISHED]:
                job.status = status
                needs_to_be_checked_got = job.needs_to_be_checked_in_lsf()
                self.assertFalse(needs_to_be_checked_got,
                                 msg=f'A job with status {status} does not need to be checked in LSF!')

    def test_determines_when_the_job_checker_seems_to_have_died(self):
        """
        Test that determines when a job needs to be checked in LSF because the previous script failed
        """

        with self.flask_app.app_context():
            job_type = 'SIMILARITY'
            params = {
                'search_type': 'SIMILARITY',
                'structure': '[H]C1(CCCN1C(=N)N)CC1=NC(=NO1)C1C=CC(=CC=1)NC1=NC(=CS1)C1C=CC(Br)=CC=1',
                'threshold': '70'
            }
            docker_image_url_must_be = 'some_url'
            job = delayed_job_models.get_or_create(job_type, params, docker_image_url_must_be)
            death_assumption_seconds = RUN_CONFIG.get('status_agent').get('death_assumption_seconds')
            last_lsf_checked_at = utils.get_utc_now() - datetime.timedelta(seconds=death_assumption_seconds)
            job.last_lsf_checked_at = last_lsf_checked_at
            job.last_lsf_check_status = 1

            job_checker_seems_to_have_died_got = job.job_checker_seems_to_have_died()
            self.assertTrue(job_checker_seems_to_have_died_got,
                            msg='When the last script execution errors and the last time it reported is greater than '
                                'the assumed dead time it must assume that the checker died.')

            job.last_lsf_checked_at = utils.get_utc_now()
            job_checker_seems_to_have_died_got = job.job_checker_seems_to_have_died()
            self.assertFalse(job_checker_seems_to_have_died_got,
                             msg='When the last script execution errors but the last time it reported is less than '
                                 'the assumed dead time it must NOT assume that the checker died.')

            job.last_lsf_checked_at = utils.get_utc_now()
            job.last_lsf_check_status = 0
            job_checker_seems_to_have_died_got = job.job_checker_seems_to_have_died()
            self.assertFalse(job_checker_seems_to_have_died_got,
                             msg='When the last script execution was successful and the last time it reported is less '
                                 'than the assumed dead time it must NOT assume that the checker died.')

            last_lsf_checked_at = utils.get_utc_now() - datetime.timedelta(seconds=death_assumption_seconds)
            job.last_lsf_checked_at = last_lsf_checked_at
            job.last_lsf_check_status = 0
            job_checker_seems_to_have_died_got = job.job_checker_seems_to_have_died()
            self.assertFalse(job_checker_seems_to_have_died_got,
                             msg='When the last script execution was successful but the last time it reported is '
                                 'greater than the assumed dead time it must NOT assume that the checker died.')
