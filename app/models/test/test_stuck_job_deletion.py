"""
Tests for the deletion of the stuck jobs
"""
import datetime
import os
import random
import shutil
import string
import unittest
from pathlib import Path

from app import create_app
from app.models import delayed_job_models
from app.models.test import utils

from app.config import RUN_CONFIG


class TestStuckJobDeletion(unittest.TestCase):
    """
    Class to test the deletion of stuck jobs jobs
    """
    TEST_RUN_DIR_NAME = 'test_run_dir'
    ABS_RUN_DIR_PATH = str(Path(TEST_RUN_DIR_NAME).resolve())
    OUT_RUN_DIR_NAME = 'test_out_dir'
    ABS_OUT_DIR_PATH = str(Path(OUT_RUN_DIR_NAME).resolve())

    def setUp(self):
        self.flask_app = create_app()
        self.client = self.flask_app.test_client()

        os.makedirs(self.ABS_RUN_DIR_PATH, exist_ok=True)
        os.makedirs(self.ABS_OUT_DIR_PATH, exist_ok=True)

    def tearDown(self):
        with self.flask_app.app_context():
            delayed_job_models.delete_all_jobs()

        shutil.rmtree(self.ABS_RUN_DIR_PATH)
        shutil.rmtree(self.ABS_OUT_DIR_PATH)

    def test_detects_when_job_is_stuck(self):
        """
        Tests that it detects when a job is stuck.
        This is when the job has been in CREATED, QUEUED, or UNKNOWN for more than the timeout time set
        in the configuration
        """

        with self.flask_app.app_context():
            stuck_assumption_seconds = RUN_CONFIG['stuck_assumption_seconds']
            all_jobs_created = self.simulate_stuck_and_unstuck_jobs()
            stuckable_statuses = [delayed_job_models.JobStatuses.CREATED, delayed_job_models.JobStatuses.QUEUED,
                                  delayed_job_models.JobStatuses.UNKNOWN]

            stuck_time = datetime.datetime.utcnow() - datetime.timedelta(seconds=stuck_assumption_seconds)
            for job in all_jobs_created:
                must_be_stuck = job.status in stuckable_statuses and job.created_at < stuck_time
                is_stuck = job.is_stuck()
                self.assertEqual(must_be_stuck, is_stuck, msg=f'Job stuck state is incorrect!')

    def test_deletes_stuck_jobs(self):
        """
        Tests that it deletes the jobs that are stuck
        """
        with self.flask_app.app_context():
            all_jobs_created = self.simulate_stuck_and_unstuck_jobs()
            ids_that_must_exist = [job.id for job in all_jobs_created if not job.is_stuck()]
            print('ids_that_must_exist: ', ids_that_must_exist)
            ids_that_must_not_exist = [job.id for job in all_jobs_created if job.is_stuck()]
            print('ids_that_must_not_exist: ', ids_that_must_not_exist)

            delayed_job_models.delete_all_stuck_jobs()

            for job_id in ids_that_must_exist:
                try:
                    delayed_job_models.get_job_by_id(job_id)
                except delayed_job_models.JobNotFoundError:
                    self.fail(f'Job with id {job_id} is not stuck so it should exist!')

            for job_id in ids_that_must_not_exist:
                with self.assertRaises(delayed_job_models.JobNotFoundError,
                                       msg=f'Job with id {job_id} is stuck so it should not exist!'):
                    delayed_job_models.get_job_by_id(job_id)

    def simulate_stuck_and_unstuck_jobs(self):
        """
        Creates jobs that are stuck and not stuck for different statuses
        """

        stuck_assumption_seconds = RUN_CONFIG['stuck_assumption_seconds']

        stuck_time = datetime.datetime.utcnow() - datetime.timedelta(seconds=stuck_assumption_seconds)
        non_expired_time = datetime.datetime.utcnow()

        stuckable_statuses = [delayed_job_models.JobStatuses.CREATED, delayed_job_models.JobStatuses.QUEUED,
                              delayed_job_models.JobStatuses.UNKNOWN]

        non_stuckable_statuses = [delayed_job_models.JobStatuses.RUNNING, delayed_job_models.JobStatuses.ERROR,
                                  delayed_job_models.JobStatuses.FINISHED]

        all_statuses = stuckable_statuses + non_stuckable_statuses
        all_jobs_created = []

        for created_at in [stuck_time, non_expired_time]:
            for status in all_statuses:
                job = self.simulate_job(created_at, status)
                all_jobs_created.append(job)

        return all_jobs_created

    def simulate_job(self, created_at, job_status):
        """
        simulates a job with a custom created at date
        :param created_at: the created at date you want for the job
        :param job_status: the status you want for the job
        """

        # create a stuck job
        job_type = 'SIMILARITY'
        params = {
            'search_type': 'SIMILARITY',
            'structure': ''.join(random.choice(string.ascii_lowercase) for i in range(10)),
            'threshold': '70'
        }
        docker_image_url = 'some_url'

        job = delayed_job_models.get_or_create(job_type, params, docker_image_url)
        job.created_at = created_at
        job.status = job_status
        delayed_job_models.save_job(job)

        return job
