#!/usr/bin/env bash
set -x

IDENTITY_FILE=$1

echo "I am going to submit the job {JOB_ID}"

EXIT_CODE=0
MAX_FAILURES=10
NUM_FAILURES=0
KEEP_TRYING=true

while [ "$KEEP_TRYING" = "true" ]
do

  ssh {LSF_USER}@{LSF_HOST} -i $IDENTITY_FILE -oStrictHostKeyChecking=no <<ENDSSH
  {SET_DOCKER_REGISTRY_CREDENTIALS}
  export LSB_JOB_REPORT_MAIL=N
  echo \$HOSTNAME >> {RUN_DIR}/submission_hostnames.txt
  bsub {RESOURCES_PARAMS} -J {JOB_ID} -o {RUN_DIR}/job_run.out -e {RUN_DIR}/job_run.err "echo \$HOSTNAME;pushd \$HOME; mkdir -p {DOCKER_SINGULARITY_DIR};popd;singularity exec {DOCKER_IMAGE_URL} /app/run_job.sh {RUN_PARAMS_FILE}"
ENDSSH

  EXIT_CODE=$?

  if [ $EXIT_CODE -gt 0 ]
  then
    NUM_FAILURES=$((NUM_FAILURES+1))
    echo 'failure' >> {RUN_DIR}/submission_hostnames.txt
  else
    echo 'success' >> {RUN_DIR}/submission_hostnames.txt
  fi

  if [ $EXIT_CODE -gt 0 ] && [ $NUM_FAILURES -lt $MAX_FAILURES ]
  then
    KEEP_TRYING=true
  else
    KEEP_TRYING=false
  fi

  sleep 1

done

echo $NUM_FAILURES >> {RUN_DIR}/num_submission_failures.txt
